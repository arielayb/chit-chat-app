import * as React from "react";
import { Button, Input } from 'reactstrap';

// load testUtils to provide fron-end/back-end communication
// with socket.io features...
const testUtils = require('./utils/testUtils.js');

interface MyComponentProps { 
    title: string; 
    framework: string; 
}
  
interface MyComponentState {
    output : string;
}

class Home extends React.Component<MyComponentProps, MyComponentState> {

    state: MyComponentState;

    constructor(props: MyComponentProps){
        super(props);
        this.state = {
            output: ''
        };
    
        //this.updateOutput = this.updateOutput.bind(this);
        this.loadIndex = this.loadIndex.bind(this);

    }

//React life cycle methods below:

    // example function, loadIndex(), will try to send 
    // packets of data throught the wire.
    loadIndex(){
        console.log("loading...");
        console.log("the text input: " + this.state.output);
        
        testUtils.send(this.state.output);
    }

    componentWillMount(){
        var indexInfo = this.state.output;
        console.log("index info: " + this.state.output);
    }

    componentDidMount() {        

    }

    componentWillUnmount(){

    }

    /*updateOutput(evt){
        this.setState({output: evt.target.value});
    }*/

/**************************************************/

    render() {
        const list = this.state.output;
        return (
          <div id="serverList">
              <div id="consoleMsg" className="fullSize" style={{width:'55%', height:'40%'}}/>
              <output name="result">{this.state.output}</output>
              <div id="toolbar">
                <Input id="basic-addon1" value={this.state.output} onChange={(evt) => this.setState({
                    output: evt.target.value
                })}></Input>
                <Button color="primary" onClick={this.loadIndex}>submit</Button>
              </div>
          </div>
        );
    }
}

export default Home;